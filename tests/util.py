# encoding: utf-8

from clang.cindex import (
    Cursor,
    TranslationUnit
)


def parse_translation_unit(src):
    return TranslationUnit.from_source('t.c', [], unsaved_files=[('t.c', src)])


def find_cursor(src, kind):
    return next(find_cursors(src, kind), None)


def find_cursors(src, kind):
    root = to_cursor(src)

    for cursor in root.walk_preorder():
        if cursor.kind == kind:
            yield cursor


def to_cursor(src):
    if isinstance(src, Cursor):
        return src
    elif isinstance(src, TranslationUnit):
        return src.cursor
    elif isinstance(src, str):
        return to_cursor(parse_translation_unit(src))


__all__ = [
    'find_cursor',
    'find_cursors',
    'parse_translation_unit',
]
