import pytest

from c2rs.trans import *
from clang.cindex import CursorKind
from util import find_cursor
from textwrap import dedent


def test_vars():
    c = find_cursor('int** x;', CursorKind.VAR_DECL)
    rs = trans_var(c)
    assert rs == 'let mut x: *mut *mut i32;', rs

    c = find_cursor('int x = 1;', CursorKind.VAR_DECL)
    rs = trans_var(c)
    assert rs == 'let mut x: i32 = 1;', rs


def test_func_arguments():
    c = find_cursor('void f(int* x, long y, short z) { }', CursorKind.FUNCTION_DECL)
    rs = trans_args(*tuple(c.get_arguments()))
    assert rs == 'x: *mut i32, y: i64, z: i16', rs


def test_func():
    c = find_cursor('int f(int a, int b) { if ((a + b) > 4) { return 1; } else { return 0; } }', CursorKind.FUNCTION_DECL)
    rs = trans_func(c)
    assert rs == dedent("""
        fn f(a: i32, b: i32) -> i32 {
            if (a + b) > 4 {
                return 1;
            } else {
                return 0;
            }
        }
        """)[1:], rs


def test_call():
    c = find_cursor('void f(int a) { f(a); }', CursorKind.CALL_EXPR)
    rs = trans_any(c)
    assert rs == 'f(a);', rs

    c = find_cursor('int f() { int x; x = f() + 2; }', CursorKind.BINARY_OPERATOR)
    rs = trans_any(c)
    assert rs == 'x = f() + 2', rs


def test_if():
    c = find_cursor('void f() { int x = 0; if (x == 1) {} }', CursorKind.IF_STMT)
    rs = trans_if(c)
    assert rs == "if x == 1 {\n\n}"

    c = find_cursor('void f() { int x = 0; if (x == 1) {} else {} }', CursorKind.IF_STMT)
    rs = trans_if(c)
    assert rs == "if x == 1 {\n\n} else {\n\n}"

    c = find_cursor('void f() { int x = 0; if (x == 1) {} else if (x == 2) {} }', CursorKind.IF_STMT)
    rs = trans_if(c)
    assert rs == "if x == 1 {\n\n} else if x == 2 {\n\n}"


def test_struct():
    c = find_cursor('''
    struct X {
        int i;
        double d[];
        unsigned long l7[7];
    };
    ''', CursorKind.STRUCT_DECL)
    rs = trans_any(c)
    assert rs == dedent('''
    struct X {
        i: i32,
        d: [f64],
        l7: [u64; 7],
    }
    ''').strip()
