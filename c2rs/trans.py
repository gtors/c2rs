#!/usr/bin/env python
# coding: utf-8

# ----------------------------------------------------------------------------
# Imports
# ----------------------------------------------------------------------------

import textwrap
from clang.cindex import *

# ----------------------------------------------------------------------------
# Constants
# ----------------------------------------------------------------------------

INDENT = " " * 4

# ----------------------------------------------------------------------------
# Globals
# ----------------------------------------------------------------------------

# Types renaming mapping
ty_map = {
    "int": "i32",
    "unsigned int": "u32",
    "long": "i64",
    "unsigned long": "u64",
    "long long": "i64",
    "unsigned long long": "u64",
    "short": "i16",
    "unsigned short": "u16",
    "char": "i8",
    "unsigned char": "u8",
    "float": "f32",
    "double": "f64",
}

# Binary operators mapping
op_map = {}

translators_registry = {}

def trans(*kinds):
    """
    Decorator which intendet to register transaltor in translators registry
    """
    def wrap(func):
        for kind in kinds:
            translators_registry[kind] = func
        return func
    return wrap

# ----------------------------------------------------------------------------
# Translators
# ----------------------------------------------------------------------------

def trans_any(node):
    if node.kind in translators_registry:
        return translators_registry[node.kind](node)
    else:
        raise RuntimeError("Translator for {} node not implemented".format(node.kind))


@trans(CursorKind.STRUCT_DECL)
def trans_struct(node):
    fields = tuple(trans_any(x) + ',' for x in node.get_children())

    if not fields:
        return f'struct {node.spelling}'
    else:
        out = ['struct %s {' % node.spelling]
        out.append(join_indent(fields))
        out.append('}')
        return join(out)


@trans(CursorKind.FUNCTION_DECL)
def trans_func(node):
    args = []
    body = None

    ret_type = trans_type(node.result_type)
    ret = '' if ret_type == 'void' else ' -> {}'.format(ret_type)

    for child in node.get_children():
        if child.kind == CursorKind.PARM_DECL:
            args.append(child)
        elif child.kind == CursorKind.COMPOUND_STMT:
            body = child
        else:
            raise RuntimeError(child.kind)

    assert body is not None
    return "fn {}({}){} {}\n".format(
        node.spelling,
        trans_args(*args),
        ret,
        trans_compound(body))


@trans(CursorKind.RETURN_STMT)
def trans_return(node):
    assert node.kind == CursorKind.RETURN_STMT
    (ret,) = node.get_children()
    return 'return {};'.format(trans_any(ret))


def trans_args(*nodes):
    assert all(node.kind == CursorKind.PARM_DECL for node in nodes)
    return ", ".join(trans_arg(node) for node in nodes)


@trans(CursorKind.PARM_DECL, CursorKind.FIELD_DECL)
def trans_arg(node):
    return "{}: {}".format(node.spelling, trans_type(node.type))


@trans(CursorKind.DECL_STMT, CursorKind.VAR_DECL)
def trans_var(node):
    assert node.kind in [CursorKind.DECL_STMT, CursorKind.VAR_DECL]

    if node.kind == CursorKind.DECL_STMT:
        out = []
        for var in node.get_children():
            out.append(trans_var(var))
        return "\n".join(out)
    elif node.kind == CursorKind.VAR_DECL:
        if next(node.get_children(), None) is None:
            return "let mut {}: {};".format(node.spelling, trans_type(node.type))
        else:
            val = trans_any(tuple(node.get_children())[-1])
            return "let mut {}: {} = {};".format(node.spelling, trans_type(node.type), val)


@trans(CursorKind.IF_STMT)
def trans_if(node):
    assert node.kind == CursorKind.IF_STMT
    (cond, body, *_else) = node.get_children()

    assert 0 <= len(_else) <= 1
    if len(_else) == 1:
        translated_else = trans_any(_else[0])
        return 'if {} {} else {}'.format(trans_any(cond), trans_any(body), translated_else)
    else:
        return 'if {} {}'.format(trans_any(cond), trans_any(body))


@trans(CursorKind.COMPOUND_STMT)
def trans_compound(node):
    assert node.kind == CursorKind.COMPOUND_STMT, node.kind
    out = []
    for child in node.get_children():
        out.append(trans_any(child))

    body = textwrap.indent("\n".join(out), INDENT)
    return "{{\n{}\n}}".format(body)


@trans(CursorKind.PAREN_EXPR)
def trans_parences(node):
    assert node.kind == CursorKind.PAREN_EXPR
    (child,) = node.get_children()
    return '({})'.format(trans_any(child))


@trans(CursorKind.BINARY_OPERATOR)
def trans_bin_op(node):
    assert node.kind == CursorKind.BINARY_OPERATOR

    (left_operand, right_operand) = node.get_children()
    return '{} {} {}{}'.format(
        trans_any(left_operand),
        op_map.get(node.operator, node.operator),
        trans_any(right_operand),
        semicolon(node))


@trans(CursorKind.UNEXPOSED_EXPR)
def trans_unexposed(node):
    assert node.kind == CursorKind.UNEXPOSED_EXPR
    (child,) = tuple(node.get_children())
    return trans_any(child)


@trans(CursorKind.CALL_EXPR)
def trans_call(node):
    assert node.kind == CursorKind.CALL_EXPR
    args = []
    for arg in node.get_arguments():
        args.append(trans_any(arg))

    return '{}({}){}'.format(
        node.spelling,
        ','.join(args),
        semicolon(node))


@trans(CursorKind.DECL_REF_EXPR)
def trans_decl_ref(node):
    assert node.kind == CursorKind.DECL_REF_EXPR, node.kind
    (ident,) = node.get_tokens()
    return ident.spelling


@trans(CursorKind.INTEGER_LITERAL)
def trans_liter(node):
    return node.literal


def trans_type(ty):
    if ty.kind == TypeKind.POINTER:
        return unwind_ptr(ty)
    elif (
        ty.kind == TypeKind.INCOMPLETEARRAY or 
        ty.kind == TypeKind.CONSTANTARRAY or
        ty.kind == TypeKind.DEPENDENTSIZEDARRAY
    ):
        return trans_array_type(ty)
    else:
        return ty_map.get(ty.spelling, ty.spelling)


def trans_array_type(ty):
    if ty.kind == TypeKind.INCOMPLETEARRAY:
        return '[%s]' % trans_type(ty.get_array_element_type())
    elif ty.kind == TypeKind.CONSTANTARRAY:
        return '[%s; %s]' % (
            trans_type(ty.get_array_element_type()),
            ty.get_array_size())
    elif ty.kind == TypeKind.DEPENDENTSIZEDARRAY:
        assert 0, 'todo'


def unwind_ptr(ty):
    assert ty.kind == TypeKind.POINTER, str(ty.kind)

    out = ['*mut']
    dead_loop = 50
    ty = ty.get_pointee()

    while ty.kind == TypeKind.POINTER:
        out.append('*mut')
        ty = ty.get_pointee()
        dead_loop -= 1
        if dead_loop == 0:
            raise RuntimeError("Deadloop detected")

    out.append(trans_type(ty))
    return " ".join(out)


def semicolon(node):
    p = find_parent(node)
    if p is None or p.kind == CursorKind.COMPOUND_STMT:
        return ';'
    else:
        return ''

def join(items):
    return "\n".join(items)

def join_indent(items):
    return textwrap.indent("\n".join(items), INDENT)

def find_parent(node):
    last = None
    for n in node.translation_unit.cursor.walk_preorder():
        if n == node:
            return last
        else:
            last = n
