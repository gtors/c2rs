#!/usr/bin/env python2

from setuptools import setup

setup(
    name='c2rs',
    version='0.0.1',
    description='Translator from C to Rust',
    license='MIT',
    author='Andrey Torsunov',
    author_email='andrey.torsunov@gmail.com',
    url='https://github.com/gtors/c2rs',
    packages=['c2rs'],
    scripts=['scripts/c2rs'],
    dependency_links=['git+https://github.com/gtors/sealang.git#egg=sealang-6.0'],
    install_requires=['sealang==6.0'],
    setup_requires=['pytest-runner',],
    tests_require=['pytest', ],
)
