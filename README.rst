C2RS
====

.. image:: https://travis-ci.org/gtors/c2rs.svg?branch=master
    :target: https://travis-ci.org/gtors/c2rs


This project is a try to write semi-automated translator from C to Rust via Clang bindings.

Dependencies
------------

- Clang == 6.0
- Python == 3.*

Installation
------------

::

    $ pip install git+https://github.com/gtors/c2rs#egg=c2rs-0.0.1
